#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 28 16:55:02 2022

@author: adenys



This file gathers functions used to assess the performances of the 2T-qutrit 
against the dephasing channel.
"""
import math
import numpy as np
from numpy.linalg import norm
import optimisation as opti
import cvxpy as cp
from scipy.linalg import orth

def small_phis_in_fock(alpha,truncation):
    '''
    states phi0, phi, phi2 from eq 11, expressed in the 2-mode Fock basis.
    These states define the 2T-qutrit
    '''
    dim = truncation**2
    phi0 = np.zeros((dim,1))
    phi1 = np.zeros((dim,1))
    phi2 = np.zeros((dim,1))
    for p in range(0,truncation):
        phi0[p**2,0] = ((-4)**p*(alpha)**(4*p))/math.sqrt(math.factorial(4*p))
        phi0[p**2+2*p,0] += phi0[p**2,0]
        for q in range(0,2*p+1):
            phi1[p**2+q,0] = alpha**(4*p)/math.sqrt(math.factorial(4*p-2*q)*math.factorial(2*q))
            phi2[p**2+q,0] = (-1)**q*phi1[p**2+q,0]
    phi0 = phi0/norm(phi0)
    phi1 = phi1/norm(phi1)
    phi2 = phi2/norm(phi2)
    return phi0,phi1,phi2


def real_orth_basis(alpha,truncation):
    ''' a real orthonoaml basis for Span(|phi0>, |phi1>, |phi2>)'''
    phi0, phi1, phi2 = small_phis_in_fock(alpha, truncation)
    mat_phi = np.zeros((truncation**2,3))
    mat_phi[:,0] = phi0[:,0]
    mat_phi[:,1] = phi1[:,0]
    mat_phi[:,2] = phi2[:,0]
    orth_basis_mat = orth(mat_phi)
    log0 = orth_basis_mat[:,0].reshape((truncation**2,1))
    log1 = orth_basis_mat[:,1].reshape((truncation**2,1))
    log2 = orth_basis_mat[:,2].reshape((truncation**2,1))
    return[log0,log1,log2]


def construct_rho(alpha,gamma_deph,truncation,dim_enc,log_states):
    rho = np.zeros((dim_enc*truncation**2,dim_enc*truncation**2))
    for k in range(0,dim_enc):
        for l in range(0,dim_enc):
            for p1 in range(0,truncation):
                for q1 in range(0,2*p1+1):
                    for p2 in range(0,truncation):
                        for q2 in range(0,2*p2+1):
                            m = p1**2+q1
                            n = p2**2+q2
                            rho[k*truncation**2+m,l*truncation**2+n] = \
                                 np.exp(-2*gamma_deph*((2*(p1-p2)+q1-q2)**2+(q1-q2)**2))*\
                                     log_states[k][m,0]*np.conjugate(log_states[l][n,0])
    return rho/dim_enc


def fidelity(alpha,gamma_deph,truncation,dim_enc,log_states):
    ''' compute the entanglement fidelity for the 2T-qutrit, for dephasing'''
    rho = construct_rho(alpha,gamma_deph,truncation,dim_enc,log_states)
    fid, X = opti.real_optimisation_sdp(mat_sdp=rho, dim_A=dim_enc, dim_B=truncation**2,
                                       dim_enc=dim_enc, int_cons_trace_on_system=1)
    return fid, X


def fid_fct_alpha(alphas,gamma_deph,truncation,
                         dim_enc = 3,fct_logical_states=real_orth_basis):
    fidelities = []
    found_alphas = []
    for alpha in alphas :
        log_states = fct_logical_states(alpha,truncation)
        try:
            fid, X = fidelity(alpha,gamma_deph,truncation,dim_enc,log_states)
        except cp.SolverError as error:
            print(error)
            break
        try :
            # check if results make sense
            np.testing.assert_array_less(fid,1+1e-9)
            np.testing.assert_array_less(0-1e-9,fid)
            partial_tr_X = opti.partial_trace_A(X, dim_enc, truncation**2)
            np.testing.assert_allclose(partial_tr_X, np.eye(truncation**2)/dim_enc,
                                       rtol=1e-4,atol=1e-4)
            found_alphas.append(alpha)
            fidelities.append(fid)
        except AssertionError as error:
            print(error)
            pass
    return found_alphas, fidelities