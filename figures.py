import numpy as np
import cvxpy as cp
from matplotlib import pyplot as plt

import code_24cell as c24
import optimisation as opti

import warnings
warnings.filterwarnings('error', category=np.ComplexWarning) #turns the warnings
# of the ComplexWarning category into errors

'''This file contains functions useful to plot the entanglement fidelity for 
the loss-channel, as a function of various parameters.'''


def generalised_plot_fct_alpha(modulation_amplitudes,func_kets,params,
                          gamma,label,fct_alphas=c24.alphas_2T,marker='+',
                          linestyle='None'):

    list_found_fidelities = []
    list_found_nbars = []
    list_found_mod_ampl = []
    for mod_ampl in modulation_amplitudes:
        twenty_four_cell = c24.TwentyFourCellConstellation(gamma, mod_ampl,fct_alphas=fct_alphas)
        kets = func_kets(**{**params,'constellation':twenty_four_cell})
        try:
            _, list_fidelities, list_nbars = \
                opti.generalised_iterative_optimisation_of_decoding_and_encoding(
                    constellation=twenty_four_cell,
                    logical_states=kets,
                    dim_enc=3,
                    nb_sdps=1)
            if len(list_fidelities) > 0:
                list_found_fidelities += [list_fidelities[-1]]
                list_found_nbars += [list_nbars[-1]]
                list_found_mod_ampl += [mod_ampl]
        except (cp.SolverError, AssertionError):
            pass

    return list_found_mod_ampl,list_found_fidelities,list_found_nbars


def final_opt_fidelity_as_a_function_of_gamma(gammas,min_alpha,max_alpha,func_logical_states,
                                              args_logical_states,dim_enc,nb_sdps,
                                              TypeConstellation,args_cons,nb_iterations=3,
                                              list_max_added_points=[10]*3,
                                              list_steps=[0.4,0.1,0.01],
                                              list_nb_series_per_subsequent_iterations=[2,1],
                                              label=None):

    list_found_fidelities = []
    list_found_nbars = []
    list_found_gammas = []
    for gamma in gammas:
        try:
            found_alphas, list_fidelities, list_nbars = \
                    opti.optimal_fid_wrt_alpha(TypeConstellation=TypeConstellation,
                                               min_alpha=min_alpha,
                                               max_alpha=max_alpha,
                                               gamma=gamma,
                                               args_cons=args_cons,
                                               func_logical_states=func_logical_states,
                                               args_logical_states=args_logical_states,
                                               dim_enc=dim_enc,
                                               nb_sdps=nb_sdps,
                                               label=label,
                                               nb_iterations=nb_iterations,
                                               list_max_added_points=[10]*3,
                                               list_steps=[0.4,0.1,0.01],
                                               list_nb_series_per_subsequent_iterations=[2,1])
            if len(list_fidelities) > 0:
                list_found_fidelities += [list_fidelities[-1]]
                list_found_nbars += [list_nbars[-1]]
                list_found_gammas += [gamma]
        except (cp.SolverError, AssertionError):
            pass
    return list_found_gammas, list_found_fidelities, list_found_nbars

def plot_opt_infid_nbar_function_of_gamma(gammas,min_alpha,max_alpha,func_logical_states,
                                          args_logical_states,dim_enc,nb_sdps,
                                          TypeConstellation,args_cons,nb_iterations=3,
                                          list_max_added_points=[10]*3,
                                          list_steps=[0.4,0.1,0.01],
                                          list_nb_series_per_subsequent_iterations=[2,1],
                                          label=None,marker='+',linestyle='None',
                                          loc='best'):

    list_found_gammas, list_found_fidelities, list_found_nbars = \
        final_opt_fidelity_as_a_function_of_gamma(gammas,min_alpha,max_alpha,func_logical_states,
                                              args_logical_states,dim_enc,nb_sdps,
                                              TypeConstellation,args_cons,nb_iterations=3,
                                              list_max_added_points=[10]*3,
                                              list_steps=[0.4,0.1,0.01],
                                              list_nb_series_per_subsequent_iterations=[2,1],
                                              label=label)
    plt.figure('infid')
    plt.plot(list_found_gammas,
             np.ones(len(np.array(list_found_fidelities)))-list_found_fidelities,
             marker=marker,linestyle=linestyle,label=label)
    plt.xlabel(r'$\gamma$')
    plt.xscale('log')
    plt.yscale('log')
    plt.ylabel('entanglement infidelity')
    plt.grid()
    plt.legend(loc=loc)
    
    plt.figure('nbar')
    plt.plot(list_found_gammas,np.real(list_found_nbars),
             marker=marker,linestyle=linestyle,label=label)
    plt.xlabel(r'$\gamma$')
    plt.xscale('log')
    plt.ylabel(r'$\overline{n}$')
    plt.grid()
    plt.legend(loc=loc)
    
    return list_found_gammas, list_found_fidelities, list_found_nbars


def final_fidelity_as_a_function_of_alpha(modulation_amplitudes,
                                          states,dim_enc,nb_sdps,
                                          TypeConstellation,args_cons):

    list_found_fidelities = []
    list_found_nbars = []
    list_found_mod_ampl = []
    for mod_ampl in modulation_amplitudes:
        constellation = TypeConstellation(**{**args_cons,'alpha':mod_ampl})
        try:
            _, list_fidelities,  list_nbars = \
                opti.generalised_iterative_optimisation_of_decoding_and_encoding(
                    constellation,
                    states,
                    dim_enc,
                    nb_sdps)
            if len(list_fidelities) > 0:
                list_found_fidelities += [list_fidelities[-1]]
                list_found_nbars += [list_nbars[-1]]
                list_found_mod_ampl += [mod_ampl]
        except (cp.SolverError, AssertionError):
            pass
    return list_found_mod_ampl,list_found_fidelities,list_found_nbars


def plot_infid_nbar_fct_of_i_sdp(constellation,states,dim_enc,
                                 nb_sdps):
    list_nb_sdps, list_fidelities, list_nbars = \
        opti.generalised_iterative_optimisation_of_decoding_and_encoding(
            constellation=constellation,
            logical_states=states,
            dim_enc=dim_enc,
            nb_sdps=nb_sdps)
    return list_nb_sdps, list_fidelities, list_nbars


def plots_random_runs(constellation,nb_sdps,nb_runs,dim_enc=2):

    list_list_nb_sdps = []
    list_list_fidelities = []
    list_list_nbars = []
    for i_run in range(nb_runs):
        logical_kets = opti.generalised_random_initial_states(constellation.nb_coh,
                                                                dim_enc=dim_enc)
        list_nb_sdps, list_fidelities, list_nbars = \
            plot_infid_nbar_fct_of_i_sdp(constellation,logical_kets,dim_enc,
                                 nb_sdps)
        list_list_nb_sdps.append(list_nb_sdps)
        list_list_fidelities.append(list_fidelities)
        list_list_nbars.append(list_nbars)

    return list_list_nb_sdps, list_list_fidelities, list_list_nbars
