#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 21 12:52:23 2021

@author: adenys

This file gathers the functions used to solve the biconvex optimisation that 
enables to optimise the entanglement fidelity.

Some of the functions contained in that file were already published in the file 
"psk.py" at https://arxiv.org/abs/2103.13945.

 
"""

import numpy as np
import cvxpy as cp
from numpy.linalg import norm
from numpy.random import normal

from matplotlib import pyplot as plt

import warnings
warnings.filterwarnings('error', category=np.ComplexWarning) #turns the warnings
# of the ComplexWarning category into errors


def real_optimisation_sdp(mat_sdp, dim_A, dim_B, dim_enc, int_cons_trace_on_system):
    '''
    Solves the real sdp
    max tr(CX)
    st tr_{A or B}(X)=I_{B or A}/dim_enc
       X >=0

    Parameters
    ----------
    mat_sdp : np.array <float>
        matrix C appearing in the SDP.
    dim_A : int
        dimension of the Hilbert space H_A where the full Hilbert space is of 
        type H_A \otimes H_B.
    dim_B : int
        dimension of the Hilbert space H_B where the full Hilbert space is of 
        type H_A \otimes H_B.
    int_cons_trace_on_system : int
        1 if tr_A(X)=I_B/dim_enc, 2 if tr_B(X)=I_A/dim_enc

    Returns
    -------
    prob.value : float
        found value for max tr(CX).
    Xreal : np.array <float>
        optimal matrix X found.

    '''
    
    if int_cons_trace_on_system == 1:
        constraints_operators_trace = projectors_basis_second_system

    elif int_cons_trace_on_system == 2:
        constraints_operators_trace = projectors_basis_first_system

    # primal SDP
    size, _ = np.shape(mat_sdp)
    X = cp.Variable((size,size), symmetric=True)

    #Define the constraints  
    n_rows, _ = np.shape(mat_sdp) 

    constraints = [X >> 0]
    #The operator >> denotes matrix inequality.

    Bkl = constraints_operators_trace(dim_A, dim_B)
    nb_op_cons = np.shape(Bkl)[0]

    for k in range(0,nb_op_cons):
        for l in range(0,nb_op_cons):
            #partial_trace(X)=I/dim_enc
            constraints += [cp.trace(Bkl[l,k,:,:]@X) == delta(k,l)/dim_enc]

    prob = cp.Problem(cp.Maximize(cp.trace(mat_sdp @ X)),constraints)

    # solve SDP
    prob.solve(solver=cp.SCS,max_iters=10000,eps=1e-8,verbose=False)

    Xreal = X.value
    return prob.value, Xreal



def optimisation_sdp(mat_sdp, dim_A, dim_B, dim_enc, int_cons_trace_on_system):
    '''
    Solves the complex sdp
    max tr(CX)
    st tr_{A or B}(X)=I_{B or A}/dim_enc
       X >=0

    Parameters
    ----------
    mat_C : np.array <complex>
        matrix appearing in the SDP.
    dim_A : int
        dimension of the Hilbert space H_A where the full Hilbert space is of 
        type H_A \otimes H_B.
    dim_B : int
        dimension of the Hilbert space H_B where the full Hilbert space is of 
        type H_A \otimes H_B.
    int_cons_trace_on_system : int
        1 if tr_A(X)=I_B, 2 if tr_B(X)=I_A

    Returns
    -------
    prob.value : complex
        found value for max tr(CX).
    Xreal : np.array <complex>
        optimal matrix X found.
    '''
    
    if int_cons_trace_on_system == 1:
        constraints_operators_trace = constraints_operators_traceA
    
    elif int_cons_trace_on_system == 2:
        constraints_operators_trace = constraints_operators_traceB
        
    # transformation of the complex SDP into a real one
    complex_mat = complex_to_real(mat_sdp)
    
    # primal SDP
    size, _ = np.shape(complex_mat)
    X = cp.Variable((size,size), symmetric=True)
    
    #Define the constraints  
    n_rows, _ = np.shape(mat_sdp) 

    # construct matrix im_tr such that the trace of im_tr*complex_matrix 
    # (where complex matrix is a 2*2 block matrices with real and imaginary blocks)
    # is twice the trace of the imaginary blocks of the matrix
    im_tr = np.zeros((2*n_rows,2*n_rows),dtype=complex)
    im_tr[0:n_rows,n_rows:2*n_rows] = -np.eye(n_rows)
    im_tr[n_rows:2*n_rows,0:n_rows] = np.eye(n_rows) 
    
    constraints = [X >> 0]
    #The operator >> denotes matrix inequality.
    
    Bkl = constraints_operators_trace(dim_A, dim_B)
    nb_op_cons = np.shape(Bkl)[0]

    for k in range(0,nb_op_cons):
        for l in range(0,nb_op_cons):
            #partial_trace(X)=I/dim_enc
            #(the values are multiplied by two to account for the transformation
            # of the complex SDP into a real one)
            constraints += [cp.trace(Bkl[l,k,:,:]@X) == 2*(delta(k,l)/dim_enc)]
            constraints += [cp.trace(im_tr@Bkl[l,k,:,:]@X) == 0]
    
    prob = cp.Problem(cp.Maximize(cp.trace(complex_mat @ X)),constraints)
    
    # solve SDP
    prob.solve(solver=cp.SCS,max_iters=10000,eps=1e-8,verbose=False)

    Xcomplex = real_to_complex(X.value)
    
    size, _ = np.shape(Xcomplex)
    return prob.value/2, Xcomplex # division by 2 to account for the 
                                  # transformation of the complex SDP into 
                                  # a real one


def optimal_fid_wrt_alpha(TypeConstellation,min_alpha,max_alpha,gamma,
                          args_cons,func_logical_states,args_logical_states,
                          dim_enc,nb_sdps,label=None,nb_iterations=3,
                          list_max_added_points=[10]*3,
                          list_steps=[0.4,0.1,0.01],
                          list_nb_series_per_subsequent_iterations=[2,1]):
    '''
    Compute the minimal entanglement fidelity of a qudit, of dimension dim_enc, 
    for alpha optimised in the range [min_alpha,max_alpha]
    
    The optimisation is done with several iterations. In each iteration, one 
    or several intervals for which the current minimal infidelity has been found 
    is devided in subintervals and a few points within these subintervals are 
    evaluated. If the entanglement fidelity cannot be computed for a particular 
    point, then other points to evaluate are added.

    Parameters
    ----------
    TypeConstellation : type of the constellation. 
        Can be TwentyFourCellConstellation or MpskConstellation.
    min_alpha : float
    max_alpha : float
    gamma : float, 
        strenght of the loss.
    args_cons : dict
        additional arguments needed to instantiate the constellation class.
    func_logical_states : function describing the logical states of the 
    constellation in an orthonormal basis, taking a constellation as argument, 
    as well as additional arguments
    args_logical_states : dict
        additional arguments taken by the function func_logical_states.
    dim_enc : int
        dimension of the qudit.
    nb_sdps : int
        number of SDPs performed to compute the entanglement fidelity.
    label : label names for plots. The default is None.
    nb_iterations : int, optional
        number of iterations performed for the optimisation. The default is 3.
    list_max_added_points : list<int>, optional
        maximum number of points to evaluate that can be added per iteration. 
        The default is [10]*3.
    list_steps : list<int>, optional
        steps used to evaluate points in intervals, for each ietartion.
        The default is [0.4,0.1,0.01].
    list_nb_series_per_subsequent_iterations : list<int>, optional
        number of intervals that will be subdivided for each iteration, after
        the first iteration. The default is [2,1].

    Returns
    -------
    list_nb_sdp, list_fidelitie, list_nbar (see description of the function
                                            generalised_iterative_optimisation_of_decoding_and_encoding) 


    '''

    def fid_alpha(alpha):
        # compute the entanglement fidelity
        constellation = TypeConstellation(**{**args_cons,'gamma':gamma,
                                             'alpha':alpha})
        logical_states = func_logical_states(**{**args_logical_states,
                                            'constellation':constellation})
        try:
            list_nb_sdps, list_fidelities, list_nbars = \
        generalised_iterative_optimisation_of_decoding_and_encoding(constellation,
                                                            logical_states,
                                                            dim_enc,
                                                            nb_sdps)
            if len(list_fidelities)>0:
                return list_fidelities[-1]
        except ValueError:
            pass
    
    def search_iteration(max_added_points,alphas,complete_serie_of_alphas,
                          found_alphas,found_fidelities):
        # perform one iteration of the optimisation 
        maximum_val_alpha = max(alphas)
        nb_added_points = 0
        for alpha in alphas :
            print('alpha',alpha)
            res = fid_alpha(alpha)
            if res is not None:
                found_alphas.append(alpha)
                found_fidelities.append(res)
            else :
                if nb_added_points < max_added_points:
                    if alpha-0.01 > min_alpha and alpha-0.01 not in complete_serie_of_alphas:
                        alphas.append(alpha-0.01)
                        complete_serie_of_alphas.append(alpha-0.01)
                        nb_added_points += 1 
                    if alpha+0.01 < maximum_val_alpha and alpha+0.01 not in complete_serie_of_alphas:
                        alphas.append(alpha+0.01)
                        complete_serie_of_alphas.append(alpha+0.01)
                        nb_added_points += 1 
    
        if len(found_alphas)>1:
            found_fidelities,found_alphas = \
                zip(*sorted(zip(found_fidelities,found_alphas),
                            key=lambda pair: pair[0],reverse=True))
            found_fidelities = list(found_fidelities)
            found_alphas = list(found_alphas)
        plt.figure()
        plt.plot(found_alphas,np.ones(len(found_fidelities))-np.array(found_fidelities),'+',
                 label=label)
        plt.yscale('log')
        plt.ylabel('infidelity')
        plt.xlabel('alpha')
        plt.legend()
        plt.tight_layout()
        plt.grid()
        plt.draw()
        return found_alphas,found_fidelities,complete_serie_of_alphas

    list_nb_series_per_iterations = [1] + list_nb_series_per_subsequent_iterations
    serie_alphas = []
    complete_serie_of_alphas = []
    found_alphas = []
    found_fidelities = []
    list_mini_serie = [min_alpha]
    list_maxi_serie = [max_alpha]
    for it in range(nb_iterations):
        for i_serie in range(list_nb_series_per_iterations[it]):
            nb_points = int((list_maxi_serie[i_serie]-list_mini_serie[i_serie])/list_steps[it])+1
            serie_alphas = list((set(serie_alphas)|set(np.linspace(list_mini_serie[i_serie],
                                            list_maxi_serie[i_serie],nb_points)))-set(complete_serie_of_alphas))
            complete_serie_of_alphas += serie_alphas
            found_alphas,found_fidelities,complete_serie_of_alphas = \
                search_iteration(list_max_added_points[it],
                                  serie_alphas,
                                  complete_serie_of_alphas,
                                  found_alphas,
                                  found_fidelities)
        if it < nb_iterations-1:
            list_mini_serie = []
            list_maxi_serie = []
            for i_subsequent_serie in range(list_nb_series_per_iterations[it+1]):
                mini = max(min_alpha,found_alphas[i_subsequent_serie]-list_steps[it])
                maxi = min(max_alpha,found_alphas[i_subsequent_serie]+list_steps[it])
                list_mini_serie.append(mini)
                list_maxi_serie.append(maxi)
    
    constellation = TypeConstellation(**{**args_cons,'gamma':gamma,
                                             'alpha':found_alphas[0]})
    logical_states = func_logical_states(**{**args_logical_states,
                                                 'constellation':constellation})
    print('final alpha', found_alphas[0])
    
    return generalised_iterative_optimisation_of_decoding_and_encoding(constellation,
                                                                       logical_states,
                                                                       dim_enc,
                                                                       nb_sdps)



def generalised_iterative_optimisation_of_decoding_and_encoding(constellation,
                                                                logical_states,
                                                                dim_enc,
                                                                nb_sdps):
    '''
    iteratively optimise the decoding and the encoding
    '''
    
    
    list_nb_sdps = []
    list_nbars = []
    
    nb_coh = constellation.nb_coh
    list_fidelities = []

    # the following part may be replaced by directly computing a random Y
    
    encoded_phi = sum([np.kron(np.reshape(np.eye(dim_enc)[:,k],(dim_enc,1))
                                          ,np.reshape(state,(len(state),1)))
                        for k,state in enumerate(logical_states)])
    encoded_phi = encoded_phi/np.sqrt(dim_enc)
    Y = encoded_phi @ np.conjugate(np.transpose(encoded_phi))
    prop_encoded_identity = partial_trace_A(Y,dim_enc,nb_coh)
    
    list_nbars += [constellation.compute_nbar(prop_encoded_identity)]

    i_sdp = 0
    while i_sdp < nb_sdps :
        # optimisation of the decoding
        i_sdp += 1
        M = constellation.construct_mat_opti_decoding(Y,dim_enc=dim_enc)
        try :
            # in all cases if a result is found, update value for X
            fid, X = optimisation_sdp(mat_sdp=M, dim_A=dim_enc, dim_B=nb_coh,
                                      dim_enc=dim_enc, int_cons_trace_on_system=1)
        except cp.SolverError as error:
            print(error)
            break
        try :
            # check if results make sense
            np.testing.assert_array_less(fid,1+1e-9)
            np.testing.assert_array_less(0-1e-9,fid)
            partial_tr_X = partial_trace_A(X, dim_enc, nb_coh)
            np.testing.assert_allclose(partial_tr_X, np.eye(nb_coh)/dim_enc,
                                       rtol=1e-4,atol=1e-4)
        # if they do, add to lists
            list_fidelities += [fid]
            list_nb_sdps += [i_sdp]
        except AssertionError as error:
            print(error)
            pass
        if i_sdp >= nb_sdps:
            break
        else:
            # optimisation of the encoding
            i_sdp += 1
            N = constellation.construct_mat_opti_encoding(X,dim_enc=dim_enc)
            try:
                # in all cases if a result is found, update value for Y
                fid2, Y = optimisation_sdp(mat_sdp=N, dim_A=dim_enc, dim_B=nb_coh,
                                           dim_enc=dim_enc,int_cons_trace_on_system=2)
            except cp.SolverError as error:
                print(error)
                break
            try :
                # check if results make sense
                np.testing.assert_array_less(fid2,1+1e-9)
                np.testing.assert_array_less(0-1e-9,fid2)
                partial_tr_Y = partial_trace_B(Y, dim_enc, nb_coh)
                np.testing.assert_allclose(partial_tr_Y, np.eye(dim_enc)/dim_enc,
                                     rtol=1e-4,atol=1e-4)
                # if they do, add to lists
                list_fidelities += [fid2]
                list_nb_sdps += [i_sdp]
                prop_encoded_identity = partial_trace_A(Y,dim_enc,nb_coh)

                list_nbars += [constellation.compute_nbar(prop_encoded_identity)]

            except AssertionError as error:
                print(error)
                pass
    return list_nb_sdps, list_fidelities, list_nbars



def constraints_operators_traceA(dim_A, dim_B):
    _Bkl = projectors_basis_second_system(dim_A,dim_B) 
          #tr_A(M)=I equiv tr(M |i>_B <j|_B = delta_ij)
    Bkl = np.zeros((((dim_B,dim_B,2*dim_A*dim_B,2*dim_A*dim_B))))
    for k in range(0,dim_B):
        for l in range(0,dim_B):
            Bkl[k,l,:,:] = complex_to_real(_Bkl[k,l,:,:])
    return Bkl


def constraints_operators_traceB(dim_A, dim_B):
    _Bkl = projectors_basis_first_system(dim_A,dim_B)
          #tr_B(N)=I equiv tr(N |i>_A <j|_A = delta_ij)
    Bkl = np.zeros((((dim_A,dim_A,2*dim_A*dim_B,2*dim_A*dim_B))))
    for k in range(0,dim_A):
        for l in range(0,dim_A):
            Bkl[k,l,:,:] = complex_to_real(_Bkl[k,l,:,:])
    return Bkl


def projectors_basis_second_system(dim_A,dim_B):
    '''
    creates the projector |i>_B <j|_B

    Parameters
    ----------
    dim_A : int
        dimension of the A Hilbert space H_A where the full Hilbert space is of 
        type H_A \otimes H_B.
    dim_B : int
        dimension of the B Hilbert space H_B where the full Hilbert space is of 
        type H_A \otimes H_B.

    Returns
    -------
    P_kl : projector.

    '''
    dim = dim_A*dim_B
    P_kl = np.zeros((((dim_B,dim_B,dim,dim))))
    for k in range(0,dim_B):
        for l in range(0,dim_B):
            for i_A in range(0,dim_A):
                P_kl[k,l,i_A*dim_B+k,i_A*dim_B+l] = 1
    return P_kl

def projectors_basis_first_system(dim_A,dim_B):
    '''
    creates the |i>_A <j|_A
    '''
    dim = dim_A*dim_B
    P_kl = np.zeros((((dim_A,dim_A,dim,dim))))
    for k in range(0,dim_A):
        for l in range(0,dim_A):
            # c'est eye et pas ones
            P_kl[k,l,k*dim_B:(k+1)*dim_B,l*dim_B:(l+1)*dim_B] = np.eye(dim_B,
                                                                       dim_B)
    return P_kl


def partial_trace_A(X,dim_A,dim_B):
    '''
    returns tr_A(X)

    Parameters
    ----------
    X : np.array <complex>
        matrix whose partial trace on the first system shall be computed.
    dim_A : int
        dimension of the A Hilbert space H_A where the full Hilbert space is of 
        type H_A \otimes H_B.
    dim_B : int
        dimension of the B Hilbert space H_B where the full Hilbert space is of 
        type H_A \otimes H_B.

    Returns
    -------
    partial_trace_A : np.array <complex>
        partial trace on the A system (first system) of X
    '''
    partial_trace_A = sum([X[k*dim_B:(k+1)*dim_B,k*dim_B:(k+1)*dim_B] for 
                           k in range(dim_A)])
    return(partial_trace_A)


def partial_trace_B(X,dim_A,dim_B):
    '''
    returns tr_B(X)

    Parameters
    ----------
    X : np.array <complex>
        matrix whose partial trace on the first system shall be computed.
    dim_A : int
        dimension of the A Hilbert space H_A where the full Hilbert space is of 
        type H_A \otimes H_B.
    dim_B : int
        dimension of the B Hilbert space H_B where the full Hilbert space is of 
        type H_A \otimes H_B.

    Returns
    -------
    partial_trace_B : np.array <complex>
        partial trace on the B system (second system) of X
    '''
    partial_trace_B = np.zeros((dim_A,dim_A),dtype=type(X[0,0]))
    for i_A in range(dim_A):
        for j_A in range(dim_A):
            partial_trace_B[i_A,j_A] = np.trace(X[i_A*dim_B:(i_A+1)*dim_B,
                                                  j_A*dim_B:(j_A+1)*dim_B])
    return(partial_trace_B)

# This function is useful to solve a complex SDP using a solver for real SDPs.
def complex_to_real(complex_mat):
    '''
    Stores an n*n complex matrix A as a 2n*2n real matrix with four blocks:
    re A   | im(A)
    -im(A) | re(A)

    Note: If A is hermitian, then the real matrix obtained is symmetric
    in : complex_mat (np.array <complex>)
    out : real_mat (np.array <float>)
    '''
    n,m = np.shape(complex_mat)
    if n!=m:
        raise ValueError('complex_mat must be a square matrix')
    else:
        real_mat = np.zeros((2*n,2*n),dtype=float)
        real_mat[0:n,0:n] = np.real(complex_mat)
        real_mat[0:n,n:2*n] = np.imag(complex_mat)
        real_mat[n:2*n,0:n] = -np.imag(complex_mat)
        real_mat[n:2*n,n:2*n] = np.real(complex_mat)
    return(real_mat)


def real_to_complex(real_mat):
    '''
    Puts a (complex) matrix written in real form back into a complex matrix, ie:
    real_to_complex(complex_to_real(complex_mat)) = complex_mat
    
    in: real_mat (np.array<float>)
    out : complex_mat (np.array <complex>)
    '''
    n,m = np.shape(real_mat)
    if n!=m:
        raise ValueError('real_mat must be a square matrix')
    elif n%2!=0:
        raise ValueError('the number of rows must be even')
    else:
        complex_mat = np.zeros((n//2,n//2),dtype=complex)
        complex_mat = real_mat[0:n//2,0:n//2]+1j* real_mat[0:n//2,n//2:n]
    return(complex_mat)


def delta(x,y):
    return int(x==y)


def generalised_random_initial_states(nb_coh,dim_enc):
    '''
    create random initial encodings

    '''
    # in psi basis
    
    re_ket_0L = normal(size=(nb_coh,1))
    im_ket_0L = normal(size=(nb_coh,1))
    ket_0L = re_ket_0L + 1j*im_ket_0L
    ket_0L = ket_0L/norm(ket_0L)
    
    initial_states = [ket_0L]
    
    for i_state in range(1,dim_enc):
        re_ket = normal(size=(nb_coh,1))
        im_ket = normal(size=(nb_coh,1))
        ket = re_ket + 1j*im_ket
        for j_state in range(i_state):
            ket = ket - np.transpose(np.conjugate(initial_states[j_state]))@ket\
                * initial_states[j_state]
        ket = ket/norm(ket)
        initial_states += [ket]
    return initial_states