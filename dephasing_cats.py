#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  4 11:26:15 2022

@author: adenys


This file gathers functions used to assess the performances of the cat qudits
against the dephasing channel.
"""

import math
import numpy as np
from numpy.linalg import norm
import optimisation as opti
import cvxpy as cp


def cats_in_fock(alpha,truncation,nb_legs,dim_enc=3):
    '''
    States from eq9 expressed in Fock basis. They define the cat qudit of 
    dimension dim_enc.
    
    nb_legs (=nb of coherent states) needs to be a multiple of three for 
    qutrits. 
    '''
    kets = np.zeros((dim_enc,truncation)) # states saved as rows
    step = nb_legs//dim_enc
    for n in range(0,(truncation)//nb_legs):
        for i in range(dim_enc):
            kets[i,n*nb_legs+i*step] = alpha**(n*nb_legs+i*step)/math.sqrt(math.factorial(n*nb_legs+i*step))
    for i in range(dim_enc):
        kets[i,:] = kets[i,:]/norm(kets[i,:])

    return [kets[i,:].reshape(truncation,1) for i in range(dim_enc)]


def construct_rho(alpha,gamma_deph,truncation,nb_legs,dim_enc=3):
    log_states = cats_in_fock(alpha,truncation,nb_legs,dim_enc=dim_enc)
    rho = np.zeros((dim_enc*truncation,dim_enc*truncation))
    for k in range(0,dim_enc):
        for l in range(0,dim_enc):
            for m in range(0,truncation):
                for n in range(0,truncation):
                    rho[k*truncation+m,l*truncation+n] = \
                        math.exp(-(gamma_deph*(n-m)**2)/2)*log_states[k][m,0]*\
                            np.conjugate(log_states[l][n,0])
    return rho/dim_enc


def fidelity(alpha,gamma_deph,truncation,nb_legs,dim_enc=3):
    ''' compute the entanglement fidelity for the cat qudit, for dephasing'''
    rho = construct_rho(alpha,gamma_deph,truncation,nb_legs,dim_enc=dim_enc)
    fid, X = opti.real_optimisation_sdp(mat_sdp=rho, dim_A=dim_enc, dim_B=truncation,
                                       dim_enc=dim_enc, int_cons_trace_on_system=1)
    return fid, X


def fid_fct_alpha(alphas,gamma_deph,truncation,nb_legs,dim_enc=3):
    fidelities = []
    found_alphas = []
    for alpha in alphas :
        try:
            fid, X = fidelity(alpha,gamma_deph,truncation,nb_legs,dim_enc=dim_enc)
        except cp.SolverError as error:
            print(error)
            break
        try :
            # check if results make sense
            np.testing.assert_array_less(fid,1+1e-9)
            np.testing.assert_array_less(0-1e-9,fid)
            partial_tr_X = opti.partial_trace_A(X, dim_enc, truncation)
            np.testing.assert_allclose(partial_tr_X, np.eye(truncation)/dim_enc,
                                       rtol=1e-4,atol=1e-4)
            found_alphas.append(alpha)
            fidelities.append(fid)
        except AssertionError as error:
            print(error)
            pass
    return found_alphas,fidelities