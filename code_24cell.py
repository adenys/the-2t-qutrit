#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 20 16:26:45 2021

@author: adenys


This file contains the functions necessary to describe bosonic codes 
within the 2T-constellation (a constellation of 24 two-mode coherent states),
for instance the 2T-qutrit.
"""

import numpy as np
from numpy.linalg import inv, norm
from scipy.linalg import sqrtm
from math import sqrt

import warnings
warnings.filterwarnings('error', category=np.ComplexWarning) #turns the warnings
# of the ComplexWarning category into errors


def alphas_2T(alpha):
    '''
    list of the complex coefficients of the 24 2-mode coherent states 
    defining the 2T-constellation (eq. 4).

    '''
    return [[alpha*np.exp((2*1j*k*np.pi)/4),
             alpha*np.exp((2*1j*l*np.pi)/4)] 
             for k in range(4) 
             for l in range(4)] +\
           [[0,sqrt(2)*alpha*np.exp((1j*np.pi)/4)*np.exp((1j*k*np.pi)/2)] 
             for k in range(4)]+\
           [[sqrt(2)*alpha*np.exp((1j*np.pi)/4)*np.exp((1j*k*np.pi)/2),0] 
             for k in range(4)]

class TwentyFourCellConstellation:
    def __init__(self,gamma,alpha,fct_alphas=alphas_2T):
        self.gamma = gamma
        self.nb_coh = 24
        self.alphas = fct_alphas(alpha)
        self.mat_vij = construct_mat_v_ij(self.alphas,self.nb_coh)
        mat_A = construct_A(self.nb_coh, self.mat_vij)
        sqrA = sqrtm(mat_A,disp=False)
        self.sqrt_A = sqrA[0]
        self.inv_sqrt_A = inv(self.sqrt_A)
        mat_B = construct_B(self.nb_coh, self.mat_vij, self.gamma)
        sqrB = sqrtm(mat_B,disp=False)
        self.sqrt_B = sqrB[0]
    def construct_mat_opti_encoding(self,previous_res_opti_deco,dim_enc=2):
        # construct M_E from eq.46 for the 2T-constellation
        return construct_mat_opti_encoding(self.nb_coh,self.gamma,
                                           previous_res_opti_deco,self.mat_vij,
                                           self.sqrt_B,self.inv_sqrt_A,
                                           dim_enc=dim_enc)
    
    def construct_mat_opti_decoding(self,previous_res_opti_enco,dim_enc=2):
        # construct N_R from eq.47 for the 2T-constellation
        return construct_mat_opti_decoding(self.nb_coh,self.gamma,
                                           previous_res_opti_enco,self.mat_vij,
                                           self.sqrt_B,self.inv_sqrt_A,
                                           dim_enc=dim_enc)
    def compute_nbar(self,encoded_state):
        # average photon number of the code
        return compute_nbar(self.nb_coh,self.mat_vij,self.inv_sqrt_A,
                            self.alphas,encoded_state)


#########  Functions to create the 2 matrices appearing in the SDPs  ##########

def construct_mat_opti_decoding(nb_coh,gamma,previous_res_opti_enco,mat_vij,
                                sqrt_B,inv_sqrt_A,dim_enc=2):
    # construct M_E from eq. 46

    # for a two-mode constellation {|a1>|b1>, ..., |a2>|b2>} M_E is equal to
    # sum_{ij} e^{- gamma v_ji(a1, b1)} e^{- gamma v_ji(a2, b2)}
    #   * I \otimes (B^{1/2} |i><i| A^{-1/2}) {X_E}^* I \otimes (A^{-1/2} |j><j| B^{1/2})
    # where v_ij, A, and B are defined in sub-functions

    # The states {|i>} form to an orthonormal basis of Span({|a1>|b1>, ..., |a2>|b2>})
    # They are given by |i> := (tau^{-1/2} |a_i>|b_i>)/sqrt(nb_coh)
    # where tau := (sum_i |ai>|bi> <a_i| <b_i|)/nb_coh

    mat_opti_decoding = np.zeros((dim_enc*nb_coh,dim_enc*nb_coh),dtype=complex)
    for i_coh in range(nb_coh):
        mat_ii = np.zeros((nb_coh,nb_coh)) # mat_ii = |i><i|
        mat_ii[i_coh,i_coh] = 1
        for j_coh in range(nb_coh):
            mat_jj = np.zeros((nb_coh,nb_coh)) # mat_jj = |j><j|
            mat_jj[j_coh,j_coh] = 1
            mat_opti_decoding += np.exp(-gamma*mat_vij[0,j_coh,i_coh])*\
                np.exp(-gamma*mat_vij[1,j_coh,i_coh])*\
                np.kron(np.eye(dim_enc),sqrt_B@mat_ii@inv_sqrt_A)@\
                    previous_res_opti_enco@\
                        np.kron(np.eye(dim_enc),inv_sqrt_A@mat_jj@sqrt_B)
    return mat_opti_decoding

def construct_mat_opti_encoding(nb_coh,gamma,previous_res_opti_deco,mat_vij,
                                sqrt_B,inv_sqrt_A,dim_enc=2):

    # construct N_R from eq. 47

    # for a two-mode constellation {|a1>|b1>, ..., |a2>|b2>} M_E is equal to
    # sum_{ij} e^{- gamma v_ji(a1, b1)} e^{- gamma v_ji(a2, b2)}
    #   * I \otimes (A^{-1/2} |j><j| B^{1/2}) {X_R}^* I \otimes (B^{1/2} |i><i| A^{-1/2})
    # where v_ij, A, and B are defined in sub-functions   


    mat_opti_encoding = np.zeros((dim_enc*nb_coh,dim_enc*nb_coh),dtype=complex)
    for i_coh in range(nb_coh):
        mat_ii = np.zeros((nb_coh,nb_coh)) # mat_ii = |i><i|
        mat_ii[i_coh,i_coh] = 1
        for j_coh in range(nb_coh):
            mat_jj = np.zeros((nb_coh,nb_coh)) # mat_jj = |j><j|
            mat_jj[j_coh,j_coh] = 1
            mat_opti_encoding += np.exp(-gamma*mat_vij[0,j_coh,i_coh])*\
                np.exp(-gamma*mat_vij[1,j_coh,i_coh])*\
                np.kron(np.eye(dim_enc),inv_sqrt_A@mat_jj@sqrt_B)@\
                    previous_res_opti_deco@\
                        np.kron(np.eye(dim_enc),sqrt_B@mat_ii@inv_sqrt_A)
    return mat_opti_encoding



# Sub-functions 

def construct_mat_v_ij(alphas,nb_coh):
    # matrix of matrices containing values for v_ij(a1, b1), v_ij(a2,b2)
    mat_vij = np.zeros((2,nb_coh,nb_coh),dtype=complex)
    for i_coh in range(nb_coh):
        for j_coh in range(nb_coh):
            # the first index corresponds to the sub-matrix considered,
            # then there is the row and the column.
            mat_vij[0,i_coh,j_coh] = v_ij(alphas[i_coh][0],alphas[j_coh][0])
            mat_vij[1,i_coh,j_coh] = v_ij(alphas[i_coh][1],alphas[j_coh][1])
    return mat_vij


def v_ij(coh_alpha,coh_beta):
    # related to <alpha|beta> by <alpha|beta> = exp(-v_ij(alpha,beta))
    return((np.abs(coh_alpha))**2+(np.abs(coh_beta))**2)/2 -\
        np.conjugate(coh_alpha)*coh_beta


def construct_A(nb_coh, mat_vij):
    # A = sum_ij e^{- v_ij} |i><j|

    tau = np.zeros((nb_coh,nb_coh),dtype=complex)
    for i in range(nb_coh):
        for j in range(nb_coh):
            tau[i,j] = np.exp(-mat_vij[0,i,j])*np.exp(-mat_vij[1,i,j])
    return tau

def construct_B(nb_coh, mat_vij, gamma):
    # B = sum_ij e^{- (1-gamma) v_ij} |i><j|

    tau_prime = np.zeros((nb_coh,nb_coh),dtype=complex)
    for i in range(nb_coh):
        for j in range(nb_coh):
            tau_prime[i,j] = np.exp(-(1-gamma)*mat_vij[0,i,j])*np.exp(-(1-gamma)*mat_vij[1,i,j])
    return tau_prime



###############################################################################

def compute_nbar(nb_coh,mat_vij,inv_sqrt_A,alphas,encoded_state):
    ''' averaged photon number per mode'''
    return(sum([encoded_state[i,j]*np.conjugate(inv_sqrt_A[m,j])*inv_sqrt_A[n,i]*\
                (alphas[n][0]*np.conjugate(alphas[m][0])+\
                 alphas[n][1]*np.conjugate(alphas[m][1]))*\
                    np.exp(-mat_vij[0,m,n]-mat_vij[1,m,n])
                for i in range(nb_coh) for j in range(nb_coh) 
                for m in range(nb_coh) for n in range(nb_coh)])/2)


###############################################################################
''' Functions to create states in the twenty-four-cell constellation'''

def the_2t_qutrit(constellation):
    # orthonormal basis for the 2T-qutrit (eq. 21)

    # depends on alpha, but not on gamma because sqrt_A in the constellation 
    # only depends on alpha
    h1 = construct_hexadec1(constellation)
    h2 = construct_hexadec2(constellation)
    h3 = construct_hexadec3(constellation)
    ket_0 = (h1 + h2 +h3)
    # print('norm',norm(ket_0))
    ket_0 = ket_0/norm(ket_0)
    ket_1 = h1+np.exp(2*1j*(np.pi/3))*h2 + np.exp(4*1j*(np.pi/3))*h3
    ket_1 = ket_1/norm(ket_1)
    ket_2 = h1+ np.exp(4*1j*(np.pi/3))*h2 + np.exp(2*1j*(np.pi/3))*h3
    ket_2 = ket_2/norm(ket_2)
    
    return [ket_0,ket_1,ket_2]


def fct_2T_qubit(constellation):
    # orthonormal basis for the 2T-qubit

    # depends on alpha, but not on gamma because sqrt_A in the constellation 
    # only depends on alpha
    h1 = construct_hexadec1(constellation)
    h2 = construct_hexadec2(constellation)
    h3 = construct_hexadec3(constellation)
    ket_1 = h1+np.exp(2*1j*(np.pi/3))*h2 + np.exp(4*1j*(np.pi/3))*h3
    ket_1 = ket_1/norm(ket_1)
    ket_2 = h1+ np.exp(4*1j*(np.pi/3))*h2 + np.exp(2*1j*(np.pi/3))*h3
    ket_2 = ket_2/norm(ket_2)
    return [ket_1,ket_2]


def construct_cjck(j,k,twenty_four_cell):
    # |c_i> is defined in eq.50 
    cjck = np.zeros((24,1),dtype=complex)
    for l in range(4):
        for m in range(4):
            cjck[4*m+l] = np.exp(-1j*j*l*(np.pi/2))*np.exp(-1j*k*m*(np.pi/2))
    cjck = express_alpha_in_psi(24, cjck, twenty_four_cell.sqrt_A)
    cjck = cjck/norm(cjck)
    return cjck


def construct_normalised_state_in_psi(twenty_four_cell,list_edges):
    '''
    in : list_edges corresponds to a list of coefficients of a non-normalised state expressed in 
    the basis |a1>|a2>, ..., |an>|bn> (where n=nb_coh) of the constellation
    "twenty_four_cell" (aka the 2T- constellation). 
    out : list of coefficients to express the normalised state in the orthonormal basis given by
    the |psi_i> := (tau^{-1/2} |a_i>|b_i>)/sqrt(nb_coh) where tau := (sum_i |ai>|bi> <a_i| <b_i|)/nb_coh
    '''
    state_in_psi = np.array(list_edges).reshape(24,1)
    state_in_psi = express_alpha_in_psi(24, state_in_psi,
                                            twenty_four_cell.sqrt_A)
    state_in_psi = state_in_psi/norm(state_in_psi)
    return state_in_psi

def construct_hexadec1(twenty_four_cell):
    # phi_0 in eq.11, expressed in |psi_i> basis
    return construct_normalised_state_in_psi(twenty_four_cell,[0]*16+[1]*8)

def construct_hexadec2(twenty_four_cell):
    # phi_1 in eq.11, expressed in |psi_i> basis
    return construct_normalised_state_in_psi(twenty_four_cell,
                                             [0,1,0,1,1,0,1,0,0,1,0,1,1,0,1,0]+[0]*8)

def construct_hexadec3(twenty_four_cell):
    # phi_2 in eq.11, expressed in |psi_i> basis
    return construct_normalised_state_in_psi(twenty_four_cell,
                                             [1,0,1,0,0,1,0,1,1,0,1,0,0,1,0,1]+[0]*8)


def express_alpha_in_psi(nb_coh,vec_alpha,sqrt_A):
    # vec_alpha = coefficients in the basis of the coherent states
    # out : coeffciients in the orthonormal basis
    vec_psi = np.zeros((nb_coh,1),dtype=complex)
    for i in range(nb_coh):
        vec_psi[i,0] = sum([vec_alpha[k,0]*sqrt_A[i,k] for k in range(nb_coh)])
    return vec_psi







