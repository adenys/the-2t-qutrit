# The 2T-qutrit

This is the python code used to perform the numerical analyses done
in the paper "The $2T$-qutrit, a two-mode bosonic qutrit", available
at [https://arxiv.org/abs/2210.16188](https://arxiv.org/abs/2210.16188).
Equations numbers present in the documentation of the code refer to 
the third version of the paper on arXiv (published version, in the
 _Quantum_ journal).

## Requirements
The python packages used are:
* numpy 
* numpy.linalg
* numpy.random
* math
* scipy.linalg
* cvxpy (for convex optimisation)
* matplotlib

The solver SCS also needs to be installed (it could be replaced 
by another solver handling SDPs by modifying the line of code  
prob.solve(solver=cp.SCS,max_iters=##,eps=##,verbose=##) appearing 
twice in the file optimisation.py). 

## Description
This python code enables to compute the entanglement fidelity of certain 
bosonic encodings for the pure-loss and the pure-dephasing channels. 

-------------------------------------------------------------------------------

* optimisation.py gathers the functions used to solve the biconvex optimisation 
that enables to optimise the entanglement fidelity. 

-------------------------------------------------------------------------------

* figures.py contains functions useful to plot the entanglement fidelity for 
the loss-channel, as a function of various parameters.


* code_24cell.py contains the functions necessary to describe bosonic codes 
within the 2T-constellation (a constellation of 24 two-mode coherent states).

* code_mpsk.py contains the functions necessary to describe bosonic codes 
within the m-phase-shift-keying constellation (a constellation of m single-mode 
coherent states lying on a circle in phase-space).

The files code_24cell.py and code_mpsk.py need to be imported to perform 
simulations of the performances against the *pure-loss* channel, for codes in 
the corresponding constellations.


To compute the entanglement fidelity of a particular code expressed as a 
superposition of multimode coherent states, one first needs to define a 
Constellation class. This Constellation class has an attribute "alphas" which
consists in the list of the complex coefficients of the multimode coherent 
states in the constellation. A particular encoding is then given as vectors 
reprsenting the logical states expressed in an orthonormal basis of Span({|alpha_k>}).

-------------------------------------------------------------------------------

* dephasing.py is specifically designed to assess the performances of the 
2T-qutrit against the *pure-dephasing* channel.

* dephasing_cats.py deals with the performances of the cat-qudits against the 
*pure-dephasing* channel.

-------------------------------------------------------------------------------

## License
This code is licensed under the MIT License.