#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  2 09:56:28 2021

@author: adenys

This file contains the functions necessary to describe bosonic codes 
within the m-phase-shift-keying constellation (a constellation of m single-mode 
coherent states lying on a circle in phase-space), for instance cat qudits.
"""

import numpy as np

import warnings
warnings.filterwarnings('error', category=np.ComplexWarning) #turns the warnings
# of the ComplexWarning category into errors

class MpskConstellation:
    def __init__(self,gamma,nb_coh,alpha):
        self.alpha = alpha
        self.gamma = gamma
        self.nb_coh = nb_coh
        s = np.sqrt(gamma)
        c = np.sqrt(1-gamma)
        self.theta = (2*np.pi)/nb_coh
        nus_salpha = create_nus(nb_coh,s*alpha,self.theta)
        nus_calpha = create_nus(nb_coh,c*alpha,self.theta)
        self.nus_alpha = create_nus(nb_coh,alpha,self.theta)
        self.loss_op = construct_loss_op(nb_coh, nus_salpha, nus_calpha,
                                         self.nus_alpha)
    def construct_mat_opti_encoding(self,previous_res_opti_deco,dim_enc=2):
        return(construct_mat_opti_encoding(self.nb_coh,
                                           previous_res_opti_deco,
                                           self.loss_op,dim_enc=dim_enc))
    def construct_mat_opti_decoding(self,previous_res_opti_enco,dim_enc=2):
        return(construct_mat_opti_decoding(self.nb_coh,
                                           previous_res_opti_enco,
                                           self.loss_op,dim_enc=dim_enc))
    def compute_nbar(self,encoded_state):
        # photon number
        return compute_nbar_from_encoded_state(encoded_state,self.alpha,
                                               self.nus_alpha,self.nb_coh)


def construct_loss_op(nb_coh,nus_salpha,nus_calpha,nus_alpha):
    '''
    construct the Kraus operators of the bosonic loss channel 
    (more precisely the restriction+corestriction of the channel to the space
     of interest : Span({|alpha_k>}) to Span({mu alpha_k}))
    The operators are expressed in the bases {|phi_k>} and {|phi_k'>}.

    Parameters
    ----------
    nb_coh : int
        number of coherent states in the PSK modulation.
    nus_salpha, nus_calpha, nus_alpha : various lists of norms

    Returns
    -------
    list_kraus_op : list <np.array<floats>>
        list containing a set of Kraus operators describing the loss channel 
        (its restriction to the space span by the |Phi_k'>).
    '''
    list_kraus_op = []
    for l_kraus in range(nb_coh):
        kraus_op = np.zeros((nb_coh,nb_coh),dtype=complex)
        for ind_in in range(nb_coh):
            ind_out = ind_in-l_kraus
            if ind_out < 0:
                ind_out += nb_coh
            kraus_op[ind_out,ind_in] = np.sqrt((nus_salpha[l_kraus]*nus_calpha[ind_out])\
                                                /nus_alpha[ind_in])
        list_kraus_op += [kraus_op]
    return list_kraus_op


def construct_mat_opti_decoding(nb_coh,previous_res_opti_enco,loss_op,
                                dim_enc=2):
    '''
    construct the matrix M_E for the optimisation of the decoding
    
    Parameters
    ----------
    previous_res_opti_enco : np.array(<complex>)
        matrix Y_E corresponding to the optimal encoding known so far 
    loss_op : list <np.array<floats>>
        Kraus operators of the (restriction+corestriction of the) loss channel 
    
    Returns
    -------
    mat_opti_decoding : np.array(<complex>)
    '''
    mat_opti_decoding = np.zeros((dim_enc*nb_coh,dim_enc*nb_coh),dtype=complex)
    for op in loss_op: # nb_coh is the number of Kraus operators 
                                   # of the loss channel
        mat_opti_decoding += ((np.kron(np.eye(dim_enc),op))@\
                             previous_res_opti_enco)@\
                             (np.kron(np.eye(dim_enc),
                                      np.transpose(
                                          np.conjugate(op))))
    return mat_opti_decoding


def construct_mat_opti_encoding(nb_coh,previous_res_opti_deco,loss_op,dim_enc=2):
    '''
    construct the matrix N_R (eq. 47) for the optimisation of the encoding
    
    Parameters
    ----------
    previous_res_opti_deco : np.array(<complex>)
        matrix X_D corresponding to the optimal encoding known so far 
    loss_op : list <np.array<floats>>
        Kraus operators of the (restriction+corestriction of the) loss channel 
    
    Returns
    -------
    mat_opti_encoding : np.array(<complex>)
    '''
    mat_opti_encoding = np.zeros((dim_enc*nb_coh,dim_enc*nb_coh),dtype=complex)
    for op in loss_op: # nb_coh is the number of Kraus operators 
                                   # of the loss channel
        mat_opti_encoding += np.kron(np.eye(dim_enc),np.transpose(np.conjugate(
                                         op)))@\
                             previous_res_opti_deco@\
                             np.kron(np.eye(dim_enc),op)
    return mat_opti_encoding


# norm of orthonormal basis states |phi_k>
def create_nus(nb_coh,alpha,theta):
    nus = np.zeros(nb_coh,dtype=complex)
    for k in range(nb_coh):
        nus[k] = sum([np.exp(-1j*p*k*theta)*np.exp(alpha**2*np.exp(1j*p*theta)) 
                        for p in range(nb_coh)])/nb_coh
    return nus


def compute_nbar_from_encoded_state(encoded_state,alpha,nus_alpha,nb_coh):
    # compute the average photon number
    nbar = 0
    for k_coh in range(nb_coh):
        nbar += encoded_state[k_coh,k_coh]*(nus_alpha[k_coh-1]/nus_alpha[k_coh])
    nbar = alpha**2 * nbar
    return nbar